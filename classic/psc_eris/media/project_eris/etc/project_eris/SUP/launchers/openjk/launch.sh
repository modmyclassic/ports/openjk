#!/bin/sh
source "/var/volatile/project_eris.cfg"
cd "/var/volatile/launchtmp"
chmod +x "openjk.arm"
echo -n 2 > "/data/power/disable"
HOME="/var/volatile/launchtmp" LD_LIBRARY_PATH="${PROJECT_ERIS_PATH}/lib" SDL_GAMECONTROLLERCONFIG="$(cat ${PROJECT_ERIS_PATH}/etc/boot_menu/gamecontrollerdb.txt)" ./openjk_sp.arm &> "${RUNTIME_LOG_PATH}/openjk_sp.log"
echo -n 1 > "/data/power/disable"
echo "launch_StockUI" > "/tmp/launchfilecommand"